#!/bin/bash

# author:  Gregory Maldonado
# contact: gmaldonado@binghamton.edu
# date:    2023-11-15

# mongodb-init.docker.sh

root=$(git worktree list | cut -d ' ' -f1)
source $root/scripts/env-init.sh

docker exec -it grape-db bash -c 'mongosh --port "$MONGO_INITDB_PORT" -- grape-db <<EOF
    var rootUser = "$MONGO_INITDB_ROOT_USERNAME";
    var rootPassword = "$MONGO_INITDB_ROOT_PASSWORD";
    var admin = db.getSiblingDB("admin");
    admin.auth(rootUser, rootPassword);
    db.createUser({user: "$MONGO_INITDB_ROOT_USERNAME", pwd: "$MONGO_INITDB_ROOT_PASSWORD", roles: [{role: "readWrite", db: "grape-db"}]});
    use
EOF'