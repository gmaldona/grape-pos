#!/bin/bash

# author:  Gregory Maldonado
# contact: gmaldonado@binghamton.edu
# date:    2023-11-15

# mongosh.docker.sh
root=$(git worktree list | cut -d ' ' -f 1)
source "$root"/scripts/env-init.sh

[ -n "$1" ] && db="$1" || db='grape-db'

docker exec -it "$db" mongosh admin -u "$MONGO_INITDB_ROOT_USERNAME" \
                                    -p "$MONGO_INITDB_ROOT_PASSWORD"
