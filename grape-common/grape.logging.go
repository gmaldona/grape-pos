package grape_common

import (
	"log"
)

func Info(msg string) {
	log.Println("[INFO] " + msg)
}

func Fine(msg string) {
	log.Println("[FINE] " + msg)
}

func Warning(msg string) {
	log.Println("[WARNING] " + msg)
}

func Fatal(msg string) {
	log.Fatalln("[FATAL] " + msg)
}

func InfoCallback(callback func() string) {
	Info(callback())
}

func FineCallback(callback func() string) {
	Fine(callback())
}

func WarningCallback(callback func() string) {
	Warning(callback())
}
