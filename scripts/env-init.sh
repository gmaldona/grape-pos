#!/bin/bash

# Grape Product

# author:  Gregory Maldonado
# contact: gmaldonado@binghamton.edu
# date:    2023-11-15

# env-init.sh

# Sets up the environment for Grape.
# This include bash functions, environment variables, etc.
# This script can be sourced from another script to inherit bash functions
# and exported variables.

root=$(git worktree list | cut -d ' ' -f1)

vars=$(cat "$root/.env" | grep -v '^#' | grep '=')
for var in $vars; do

   # given a defined var in $root/.env:
   #        eg. MONGO_INITDB_ROOT_USERNAME=mongoadmin
   # will automatically export each variable using gnu cut the delimiter '='
   export "$(echo "$var" | cut -d '=' -f 1)=$(echo "$var" | cut -d '=' -f 2)"

done