#!/bin/bash

# Grape Product

# author:  Gregory Maldonado
# contact: gmaldonado@binghamton.edu
# date:    2023-11-15

# ports.sh

# A grep for all ports within $root/.env.
# This script is intended to find ports that if needed to create
# a new microservice, a new port can be opened easily.
# This script works with piping to sort to get a cleaner output.

# ./ports.sh | sort -u

# Could just read the $root/.env to find the next available port
# but fun scripting because why not.

root=$(git worktree list | cut -d ' ' -f1)
vars=$(cat "$root/.env" | grep -v '^#' | grep '=' | grep PORT)
for var in $vars; do

   echo "$var" | cut -d '=' -f 2

done