#!/bin/bash

# Grape Product

# author:  Gregory Maldonado
# contact: gmaldonado@binghamton.edu
# date:    2023-11-15

# build-app.sh

# Grape uses a micro-service architecture to build the complete application.
# Docker Compose is the tool to ensure all docker VM are up and running.

root=$(git worktree list | cut -d ' ' -f 1)
source "$root"/scripts/env-init.sh

pushd "$root"

   docker-compose -f "$root/docker-compose.yml" up -d --build

popd