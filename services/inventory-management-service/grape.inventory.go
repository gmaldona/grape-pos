package main

/* Grape Product
 *
 * author:  Gregory Maldonado
 * contact: gmaldonado@binghamton.edu
 * date:    2023-11-15
 */

import (
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	grape "grape/grape-common"
	grapedb "grape/grape-db"
	"net/http"
	"os"
	"time"
)

var (
	_ *mongo.Client
)

const base = "/api/v1"

func health(response http.ResponseWriter, _ *http.Request) {
	_, err := response.Write([]byte(time.Now().String()))
	if err != nil {
		return
	}
	response.WriteHeader(http.StatusOK)
}

func main() {
	db, err := grapedb.New(grapedb.DBClient{
		DbScheme:      "mongodb",
		ConnectionUri: "127.0.0.1:27017",
	})
	if err != nil {
		grape.Fatal(err.Error())
	}

	_ = db.Client()

	router := mux.NewRouter()

	port := os.Getenv("port")

	router.HandleFunc(base+"/health", health).
		Methods("GET")

	grape.Info("Starting microservice on port " + port)
	err = http.ListenAndServe(":"+port, router)
	if err != nil {
		return
	}
}
