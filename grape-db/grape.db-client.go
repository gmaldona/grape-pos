package grape_db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

/**
*  Grape Product
*
* author:  Gregory Maldonado
* contact: gmaldonado@binghamton.edu
* date:    2023-11-15
*
 */

/**
* DBClient is scoped to only MongoDB until there is a need to abstract it out
* for dependency injection. As of 2023-11-17, MongoDB is the only DB Grape is
* using.
 */

type DBClient struct {
	DbScheme      string
	ConnectionUri string
}

func New(client DBClient) (*mongo.Database, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	connection, err := mongo.Connect(ctx, options.Client().
		ApplyURI(client.DbScheme+"://"+client.ConnectionUri))

	if err != nil {
		return nil, err
	}

	defer func() {
		if err = connection.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()

	collection := connection.Database("grape_db")
	return collection, nil
}
